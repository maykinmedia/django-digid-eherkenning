import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

EHERKENNING_DS_XSD = os.path.join(BASE_DIR, "xsd", "eherkenning-dc.xml")
