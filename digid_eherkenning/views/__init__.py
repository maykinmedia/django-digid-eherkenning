from .digid import DigiDAssertionConsumerServiceView, DigiDLoginView  # noqa
from .eherkenning import (  # noqa
    eHerkenningAssertionConsumerServiceView,
    eHerkenningLoginView,
)
