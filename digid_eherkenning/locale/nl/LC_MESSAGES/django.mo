��    	      d      �       �      �   @   �   3   1  :   e  1   �  7   �  C   
  D   N  g  �     �  O     =   \  D   �  )   �  /   	  D   9  K   ~                                          	     (new account) A technical error occurred from %(ip)s during %(service)s login. Login failed due to no BSN being returned by DigiD. Login failed due to no RSIN being returned by eHerkenning. Login to DigiD did not succeed. Please try again. Login to eHerkenning did not succeed. Please try again. The %(service)s login from %(ip)s did not succeed or was cancelled. User %(user)s%(new_account)s from %(ip)s logged in using %(service)s Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-23 16:55+0200
Last-Translator: Alexander Schrijver <alexander@maykinmedia.nl>
Language-Team: Maykin Media <info@maykinmedia.nl>
Language: NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  (nieuw account) Er is een technische fout opgetreden tijdens de %(service)s-login vanaf %(ip)s. Login mislukt doordat er geen BSN is teruggegeven door DigiD. Login mislukt doordat er geen RSIN is teruggegeven door eHerkenning. DigiD-login mislukt. Probeer het nogmaals eHerkenning-login mislukt. Probeer het nogmaals De %(service)s-login van %(ip)s is niet geslaagd of was geannuleerd. Gebruiker %(user)s%(new_account)s is met %(service)s ingelogd vanaf %(ip)s. 